#!/usr/bin/env python
#
# EOS_Tables: manages EOS tables for THC
# Loosely based on Filippo Galeazzi's Matlab scripts
# Copyright (C) 2016, David Radice <dradice@caltech.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

amu_cgs       = 1.66053873e-24
c_cgs         = 29979245800.0
fermi_cgs     = 1.e-13
kb_cgs        = 1.3806488e-23
kb_MeV        = 8.6173324e-11
rad_const_cgs = 7.5657e-15
unit_ergs2MeV = 624150.93432601797
unit_Mev2ergs = 1.6021765649999999e-6
unit_Mev2K    = 11604525006.17
unit_K2Mev    = 8.617328149741e-11
