#!/usr/bin/env python
#
# EOS_Tables: manages EOS tables for THC
# Loosely based on Filippo Galeazzi's Matlab scripts
# Copyright (C) 2016, David Radice <dradice@caltech.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

hydro = [
    "cs2",
    "density",
    "depsdT_rho",
    "dpdeps_rhoye",
    "dpdrho_epsye",
    "entropy",
    "internalEnergy",
    "mass_factor",
    "pressure",
    "temperature",
    "version",
    "ye",
]

weak = [
    "Abar",
    "Xa",
    "Xh",
    "Xn",
    "Xp",
    "Zbar",
    "density",
    "mass_factor",
    "mu_e",
    "mu_n",
    "mu_p",
    "temperature",
    "version",
    "ye",
]

comp = [
    "Abar",
    "X3he",
    "X4li",
    "XL",
    "Xa",
    "Xd",
    "Xh",
    "Xn",
    "Xp",
    "Zbar",
    "density",
    "mass_factor",
    "temperature",
    "version",
    "ye",
]
